#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 18:09:39 2020

@author: kapu
"""

#-------------------------------------------------------------------------------------------

# The BeautifulSoup library parses the element trees of the XML input files
from bs4 import BeautifulSoup

# The re module provides regular expressions matching operations
import re

# The glob module generates lists of files matching given patterns (file paths)
import glob

import os

# The pathlib module offers a set of classes to handle filesystem paths
from pathlib import Path

# The rdflib library allows interactions with RDF
import rdflib

import urllib.request
from urllib.error import HTTPError

# the datetime module allows working with dates, times and time intervals
from datetime import datetime

import requests
from urllib3.exceptions import ProtocolError

#-------------------------------------------------------------------------------------------

__author__ = "Diandra Cristache"
__copyright__ = " "
__credits__ = ["Cambridge Digital Library"]
__license__ = "Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) "
__maintainer__ = "Diandra Cristache"
__email__ = "diandra.cristache@etu.univ-tours.fr"
__status__ = "Production"

#-------------------------------------------------------------------------------------------

delimiter = ","

# -------------------------------------------------------------------
# -------------------------------------------------------------------
# ---------------------- POLONSKY METADATA --------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
                
print("FILENAME",delimiter,"TITLE",delimiter,"NAME",delimiter,"ROLE",delimiter,"FORM",delimiter,"SUPPORT",delimiter,"PLACE",delimiter,"CENTURY",file=open("../csv/mss_quest_polonsky_graphs.csv", "w"))

corpus = glob.glob('../xml/xml_polonsky/*/*.xml')

uriLOC = "http://www.loc.gov/marc/relators/relacode.html"
html_content = requests.get(uriLOC).text
                    
for file in corpus:

    # Open the original file 
    # and store the reference in the variable 'file_open'
    file_open = open(file)

    # Read the file content 
    # and store it in the variable 'file_content'
    file_content = file_open.read()

    # Give BeautifulSoup access to the file content 
    # and store the access in the variable 'soup'
    soup = BeautifulSoup(file_content, "xml")

    # Close the original file
    file_open.close()
    
    # Pull out the name of current file
    # from the local file path
    filename=os.path.splitext(os.path.basename(file))[0]
        
    # Parse each file of the corpus, while it is open in reading mode
    with open(file, 'r', encoding="utf-8") as content:
        
        # ----------------------------------------------------
        # ---------------- PULL OUT THE TITLE ----------------
        # ----------------------------------------------------
        
        mssTitle = soup.select('titleStmt title')
        for a in mssTitle:
            title = ' '.join(a.get_text().split())
                
        # ----------------------------------------------------
        # ---------------- PULL OUT THE DATE ----------------
        # ----------------------------------------------------
        
        mssOrigDate = soup.find_all('origDate')
        
        for a in mssOrigDate:
            
            if a:

                # ----------------DATE ----------------
                
                try:
                    when = a['when']
                except KeyError:
                    when = None
                    
                if when:
                    when = re.sub('-.*$', '', when)
                    origCent = "{}th century".format(int(str(when).zfill(4)[:2])+1)
                
                else:
                
                    # ----------------MINIMUM DATE ----------------
                    
                    try: 
                        minDate = a['notBefore']
                    except KeyError:
                        minDate = None
                    if minDate:
                        minDate = re.sub('-.*$', '', minDate)
                        origCent = "{}th century".format(int(str(minDate).zfill(4)[:2])+1)
                        
                    else:
            
                        # --------------- MAXIMUM DATE ----------------
                        
                        try: 
                            maxDate = a['notAfter']
                        except KeyError:
                            maxDate = None
                            
                        if maxDate:
                            maxDate = re.sub('-.*$', '', maxDate)
                            origCent = "{}th century".format(int(str(maxDate).zfill(4)[:2])+1)
                        
                        else:
                            origCent = None
                    
            else:
                origCent = None
                
        # ----------------------------------------------------
        # ---------------- PULL OUT THE PLACE ----------------
        # ----------------------------------------------------
        
        mssPlace = soup.find_all('origPlace')
        
        if mssPlace:
            for a in mssPlace:
                origPlace = ' '.join(a.get_text().replace(',', ' -').split())
        else:
            origPlace = None
            
        # ----------------------------------------------------
        # ---------------- PULL OUT THE FORM ----------------
        # ----------------------------------------------------

        mssForm = soup.select('msDesc physDesc objectDesc')
            
        if mssForm:
            for a in mssForm:
                try:
                    form = a['form']
                except KeyError:
                    form = None
        else:
            form = None
        # ----------------------------------------------------
        # ---------------- PULL OUT THE SUPPORT ----------------
        # ----------------------------------------------------
        
        mssSupport = soup.select('msDesc physDesc objectDesc supportDesc')
        
        if mssSupport:
            for a in mssSupport:
                try:
                    support = a['material']
                    if support == 'chart':
                        support = 'paper'
                    if support == 'perg':
                        support = 'parchment'
                except KeyError:
                    support = None
        else:
            support = None
        
        # ----------------------------------------------------
        # ---------------- PULL OUT THE PERSONS --------------
        # ----------------------------------------------------
        
        mssNames = soup.find_all('name',{'key':re.compile(r'.*')})

        if mssNames:
            
            # ----------------------------------------------------
            # ---------------- PULL OUT THE NAME ----------------
            # ----------------------------------------------------
            
            for name in mssNames:
                
                try:
                    nameKey = str(name['key']).replace('person_v','')    
                except KeyError:
                    nameKey = None
                   
                if nameKey is not None:
                    
                    try:
                        uriRDF = "https://viaf.org/viaf/{}/rdf.xml".format(nameKey)
                        request = requests.get(uriRDF)
                    
                        if request.status_code == 200:
                            g = rdflib.Graph()
                            parse = g.parse(uriRDF)
                            
                            ask_lc = parse.query(
                                """ASK {?x skos:prefLabel ?persName ;
                                        skos:inScheme <http://viaf.org/authorityScheme/LC> .
                                }""")
                            ask_skos = parse.query(
                                """ASK {?x skos:prefLabel ?persName ;
                                        rdf:type <http://www.w3.org/2004/02/skos/core#Concept> .
                                }""")
                            ask_sudoc = parse.query(
                                    """ASK {?x skos:prefLabel ?persName ;
                                            skos:inScheme <http://viaf.org/authorityScheme/SUDOC> .
                                    }""")
                            
                            if ask_lc:
                                    qres_name = parse.query(
                                        """SELECT ?persName
                                           WHERE {
                                              ?x skos:prefLabel ?persName ;
                                              skos:inScheme <http://viaf.org/authorityScheme/LC> .
                                           }""")
                                    for row in qres_name:
                                        persName = "%s" %row
                                        persName = persName.replace(","," ")
    
                            elif ask_skos:
                                qres_name = parse.query(
                                    """SELECT ?persName
                                       WHERE {
                                          ?x skos:prefLabel ?persName ;
                                          rdf:type <http://www.w3.org/2004/02/skos/core#Concept> .
                                       }""")
                                for row in qres_name:
                                    persName = "%s" %row
                                    persName = persName.replace(","," ")
                                    
                            elif ask_sudoc:
                                qres_name = parse.query(
                                    """SELECT ?persName
                                       WHERE {
                                          ?x skos:prefLabel ?persName ;
                                          skos:inScheme <http://viaf.org/authorityScheme/SUDOC> .
                                       }""")
                                for row in qres_name:
                                    persName = "%s" %row
                                    persName = persName.replace(","," ")
    
                            else:
                                persName = None
                            
                        else:
                            persName = None
                            
                    except requests.ConnectionError as e:
                        print("Debug exception : ",e)
                        print("url: ",uri)
                        print("code: ",request.status_code)
                        persName = None
                
                # ----------------------------------------------------
                # ---------------- PULL OUT THE ROLE ----------------
                # ----------------------------------------------------
                
                try:
                    nameRole = str(name['role'])
                except KeyError:
                    nameRole = None
                    
                if nameRole is not None:
                    
                    
                    soup = BeautifulSoup(html_content, "lxml")
                        
                    output_rows = []
                    for table_row in soup.findAll('tr'):
                        columns = table_row.findAll('td')
                        output_row = []
                        for column in columns:
                            output_row.append(column.text)
                        output_rows.append(output_row)
                        for a in output_row:
                            abbrRole = output_row[0]
                            extRole = output_row[1]
                                
                        if (nameRole == abbrRole):
                            nameRole = extRole
                
                # ----------------------------------------------------
                # ---------------- PRINT RESULTS ----------------
                # ----------------------------------------------------
                print(filename,delimiter,title,delimiter,persName,delimiter,nameRole,delimiter,form,delimiter,support,delimiter,origPlace,delimiter,origCent,file=open("../csv/mss_quest_polonsky_graphs.csv", "a"))
            
        else:
            persName = None
            
            
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# ---------------------- BODLEIAN METADATA --------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
                    
print("FILENAME",delimiter,"TITLE",delimiter,"NAME",delimiter,"ROLE",delimiter,"FORM",delimiter,"SUPPORT",delimiter,"PLACE",delimiter,"CENTURY",file=open("../csv/mss_quest_bodleian_graphs.csv", "w"))

corpus = glob.glob('../xml/xml_bodleian/*/*.xml')

uriLOC = "http://www.loc.gov/marc/relators/relacode.html"
html_content = requests.get(uriLOC).text

def centuryFromYear(year):
    return(year-1) // 100+1
                    
for file in corpus:

    # Open the original file 
    # and store the reference in the variable 'file_open'
    file_open = open(file)

    # Read the file content 
    # and store it in the variable 'file_content'
    file_content = file_open.read()

    # Give BeautifulSoup access to the file content 
    # and store the access in the variable 'soup'
    soup = BeautifulSoup(file_content, "xml")

    # Close the original file
    file_open.close()
    
    # Pull out the name of current file
    # from the local file path
    filename=os.path.splitext(os.path.basename(file))[0]
        
    # Parse each file of the corpus, while it is open in reading mode
    with open(file, 'r', encoding="utf-8") as content:
        
        # ----------------------------------------------------
        # ---------------- PULL OUT THE TITLE ----------------
        # ----------------------------------------------------
        
        mssTitle = soup.select('titleStmt title')
        
        for a in mssTitle:
            title = ' '.join(a.get_text().split())
        
        
                
        # ----------------------------------------------------
        # ---------------- PULL OUT THE DATE ----------------
        # ----------------------------------------------------
        
        mssOrigDate = soup.find_all('origDate')
        
        for a in mssOrigDate:
            
            if a:

                # ----------------DATE ----------------
                
                try:
                    when = a['when']
                except KeyError:
                    when = None
                    
                if when:
                    when = re.sub('-.*$', '', when)
                    origCent = "{}th century".format(int(str(when).zfill(4)[:2])+1)
                
                else:
                
                    # ----------------MINIMUM DATE ----------------
                    
                    try: 
                        minDate = a['notBefore']
                    except KeyError:
                        minDate = None
                    if minDate:
                        minDate = re.sub('-.*$', '', minDate)
                        origCent = "{}th century".format(int(str(minDate).zfill(4)[:2])+1)
                        
                    else:
            
                        # --------------- MAXIMUM DATE ----------------
                        
                        try: 
                            maxDate = a['notAfter']
                        except KeyError:
                            maxDate = None
                            
                        if maxDate:
                            maxDate = re.sub('-.*$', '', maxDate)
                            origCent = "{}th century".format(int(str(maxDate).zfill(4)[:2])+1)
                        
                        else:
                            origCent = None
                    
            else:
                origCent = None
            
        # ----------------------------------------------------
        # ---------------- PULL OUT THE PLACE ----------------
        # ----------------------------------------------------
        
        mssPlace = soup.find_all('country',{'key':re.compile(r'.*')})
        
        if mssPlace:
            for a in mssPlace:
                origPlace = ' '.join(a.get_text().replace(',', ' -').split())
        else:
            origPlace = None
            
        # ----------------------------------------------------
        # ---------------- PULL OUT THE FORM ----------------
        # ----------------------------------------------------

        mssForm = soup.select('msDesc physDesc objectDesc')
            
        if mssForm:
            for a in mssForm:
                try:
                    form = a['form']
                except KeyError:
                    form = None
        else:
            form = None
        # ----------------------------------------------------
        # ---------------- PULL OUT THE SUPPORT ----------------
        # ----------------------------------------------------
        
        mssSupport = soup.select('msDesc physDesc objectDesc supportDesc')
        
        if mssSupport:
            for a in mssSupport:
                try:
                    support = a['material']
                    if support == 'chart':
                        support = 'paper'
                    if support == 'perg':
                        support = 'parchment'
                except KeyError:
                    support = None
        else:
            support = None
            
        # ----------------------------------------------------
        # ---------------- PULL OUT THE PERSONS --------------
        # ----------------------------------------------------
        
        mssNames = soup.find_all('persName',{'key':re.compile(r'.*')})
        
        if mssNames:
            
            # ----------------------------------------------------
            # ---------------- PULL OUT THE PERSONS ----------------
            # ----------------------------------------------------
            
            for name in mssNames:
                try:
                    nameKey = str(name['key']).replace('person_','')    
                except KeyError:
                    nameKey = None
                    
                if nameKey is not None:
                    
                    try:
                        uriRDF = "https://viaf.org/viaf/{}/rdf.xml".format(nameKey)
                        request = requests.get(uriRDF)
                    
                        if request.status_code == 200:
                            g = rdflib.Graph()
                            parse = g.parse(uriRDF)
                            
                            ask_lc = parse.query(
                                """ASK {?x skos:prefLabel ?persName ;
                                        skos:inScheme <http://viaf.org/authorityScheme/LC> .
                                }""")
                            ask_skos = parse.query(
                                """ASK {?x skos:prefLabel ?persName ;
                                        rdf:type <http://www.w3.org/2004/02/skos/core#Concept> .
                                }""")
                            ask_sudoc = parse.query(
                                    """ASK {?x skos:prefLabel ?persName ;
                                            skos:inScheme <http://viaf.org/authorityScheme/SUDOC> .
                                    }""")
                            
                            if ask_lc:
                                    qres_name = parse.query(
                                        """SELECT ?persName
                                           WHERE {
                                              ?x skos:prefLabel ?persName ;
                                              skos:inScheme <http://viaf.org/authorityScheme/LC> .
                                           }""")
                                    for row in qres_name:
                                        persName = "%s" %row
                                        persName = persName.replace(","," ")
    
                            elif ask_skos:
                                qres_name = parse.query(
                                    """SELECT ?persName
                                       WHERE {
                                          ?x skos:prefLabel ?persName ;
                                          rdf:type <http://www.w3.org/2004/02/skos/core#Concept> .
                                       }""")
                                for row in qres_name:
                                    persName = "%s" %row
                                    persName = persName.replace(","," ")
                                    
                            elif ask_sudoc:
                                qres_name = parse.query(
                                    """SELECT ?persName
                                       WHERE {
                                          ?x skos:prefLabel ?persName ;
                                          skos:inScheme <http://viaf.org/authorityScheme/SUDOC> .
                                       }""")
                                for row in qres_name:
                                    persName = "%s" %row
                                    persName = persName.replace(","," ")
    
                            else:
                                persName = None
                            
                        else:
                            persName = ' '.join(name.get_text().replace(',', ' -').split())
                            
                    except requests.ConnectionError as e:
                        print("Debug exception : ",e)
                        print("url: ",uri)
                        print("code: ",request.status_code)
                        persName = None
                    
                # ----------------------------------------------------
                # ---------------- PULL OUT THE ROLE ----------------
                # ----------------------------------------------------
                
                try:
                    nameRole = str(name['role'])
                except KeyError:
                    nameRole = None
                    
                if nameRole is not None:
                    
                    soup = BeautifulSoup(html_content, "lxml")
                        
                    output_rows = []
                    for table_row in soup.findAll('tr'):
                        columns = table_row.findAll('td')
                        output_row = []
                        for column in columns:
                            output_row.append(column.text)
                        output_rows.append(output_row)
                        for a in output_row:
                            abbrRole = output_row[0]
                            extRole = output_row[1]
                                
                        if (nameRole == abbrRole):
                            nameRole = extRole
                
                    
                # ----------------------------------------------------
                # ---------------- PRINT RESULTS ----------------
                # ----------------------------------------------------
                print(filename,delimiter,title,delimiter,persName,delimiter,nameRole,delimiter,form,delimiter,support,delimiter,origPlace,delimiter,origCent,file=open("../csv/mss_quest_bodleian_graphs.csv", "a"))
                
            else:
                persName = None
                
                
