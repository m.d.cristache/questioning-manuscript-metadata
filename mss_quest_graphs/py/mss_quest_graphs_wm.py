#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""

# The BeautifulSoup library parses the element trees of the XML input files
from bs4 import BeautifulSoup

# The re module provides regular expressions matching operations
import re

# The glob module generates lists of files matching given patterns (file paths)
import glob

import os

# The pathlib module offers a set of classes to handle filesystem paths
from pathlib import Path

# The rdflib library allows interactions with RDF
import rdflib

import urllib.request
from urllib.error import HTTPError

# the datetime module allows working with dates, times and time intervals
from datetime import datetime

import requests

#-------------------------------------------------------------------------------------------

__author__ = "Diandra Cristache"
__copyright__ = " "
__credits__ = ["Cambridge Digital Library"]
__license__ = "Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) "
__maintainer__ = "Diandra Cristache"
__email__ = "diandra.cristache@etu.univ-tours.fr"
__status__ = "Production"

#-------------------------------------------------------------------------------------------

delimiter = ","
                    
print("FILENAME",delimiter,"TITLE",delimiter,"PLACE",delimiter,"FORM",delimiter,"SUPPORT",delimiter,"WATERMARK",delimiter,"CENTURY",file=open("../csv/mss_quest_graphs_wm.csv", "w"))

corpus = glob.glob('../xml/xml_polonsky/*/*.xml')

for file in corpus:

    # Open the original file 
    # and store the reference in the variable 'file_open'
    file_open = open(file)

    # Read the file content 
    # and store it in the variable 'file_content'
    file_content = file_open.read()

    # Give BeautifulSoup access to the file content 
    # and store the access in the variable 'soup'
    soup = BeautifulSoup(file_content, "xml")

    # Close the original file
    file_open.close()
    
    # Pull out the name of current file
    # from the local file path
    filename=os.path.splitext(os.path.basename(file))[0]
        
    # Parse each file of the corpus, while it is open in reading mode
    with open(file, 'r', encoding="utf-8") as content:
        
        # ----------------------------------------------------
        # ---------------- PULL OUT THE TITLE ----------------
        # ----------------------------------------------------
        
        mssTitle = soup.select('titleStmt title')
    
        for a in mssTitle:
            
            title = ' '.join(a.get_text().split())
                
        # ----------------------------------------------------
        # ---------------- PULL OUT THE DATE ----------------
        # ----------------------------------------------------
            
        mssOrigDate = soup.find_all('origDate')
        
        for a in mssOrigDate:
            
            if a:
        
                # ----------------DATE ----------------
                
                try:
                    when = a['when']
                except KeyError:
                    when = None
                    
                if when:
                    when = re.sub('-.*$', '', when)
                    origCent = "{}th century".format(int(str(when).zfill(4)[:2])+1)
                
                else:
                
                    # ----------------MINIMUM DATE ----------------
                    
                    try: 
                        minDate = a['notBefore']
                    except KeyError:
                        minDate = None
                    if minDate:
                        minDate = re.sub('-.*$', '', minDate)
                        origCent = "{}th century".format(int(str(minDate).zfill(4)[:2])+1)
                        
                    else:
            
                        # --------------- MAXIMUM DATE ----------------
                        
                        try: 
                            maxDate = a['notAfter']
                        except KeyError:
                            maxDate = None
                            
                        if maxDate:
                            maxDate = re.sub('-.*$', '', maxDate)
                            origCent = "{}th century".format(int(str(maxDate).zfill(4)[:2])+1)
                        
                        else:
                            origCent = None
                    
            else:
                origCent = None
                
        # ----------------------------------------------------
        # ---------------- PULL OUT THE PLACE ----------------
        # ----------------------------------------------------
        
        mssPlace = soup.find_all('origPlace')
        
        if mssPlace:
            for a in mssPlace:
                placeName = ' '.join(a.get_text().replace(',', ' -').split())
        else:
            placeName = None
            
        # ----------------------------------------------------
        # ---------------- PULL OUT THE FORM ----------------
        # ----------------------------------------------------

        mssForm = soup.select('msDesc physDesc objectDesc')
            
        for a in mssForm:
            if a:
                try:
                    form = a['form']
                except KeyError:
                    form = None
            else:
                form = None
        # ----------------------------------------------------
        # ---------------- PULL OUT THE SUPPORT ----------------
        # ----------------------------------------------------
        
        mssSupport = soup.select('msDesc physDesc supportDesc')
        
        for a in mssSupport:
            if a:
                try:
                    support = a['material']
                    if support == 'chart':
                        support = 'paper'
                    if support == 'perg':
                        support = 'parchment'
                except KeyError:
                    support = None
            else:
                support = None
                
        # ----------------------------------------------------
        # ---------------- PULL OUT THE WATERMARK ------------
        # ----------------------------------------------------
        
        mssWM = soup.find_all('term',{'type':'watermarkMotif'})
        
        if mssWM:
            for a in mssWM:
                watermark = ' '.join(a.get_text().split())
                
                # ----------------------------------------------------
                # ---------------- PRINT RESULTS ----------------
                # ----------------------------------------------------
                    
                print(filename,delimiter,title,delimiter,placeName,delimiter,form,delimiter,support,delimiter,watermark,delimiter,origCent,file=open("../csv/mss_quest_graphs_wm.csv", "a"))
        
        else:
            watermark = None
                
            #
