# Creating and questioning research-oriented digital outputs to manuscript metadata: a case-based methodological investigation
***


The paper *Creating and questioning research-oriented digital outputs to manuscript metadata: a case-based methodological investigation* leads a methodological investigation into the process of modeling, creating and questioning manuscript XML-TEI metadata.  
  
To support this investigation with a practical example, a computational solution was developed for processing the manuscript metadata produced by the Polonsky Foundation Greek Manuscripts Project. This solution consists of a group of python scripts that parse the source metadata and output machine-readable spreadsheets; the spreadsheets are then fed into tools for distant reading and data networking and result in research-oriented digital outputs, such as relational graphs and geographic maps.  

This repository contains the input XML-TEI metadata, the python scripts and the output CSV spreadsheets that are behind the relational graphs and geographic maps presented in the paper.

### Structure of data


 The repository contains two main folders:  
 
* `mss_quest_graphs` contains the scripts designed to output data suitable for creating relational graphs  

    * `xml` contains the XML-TEI input metadata records

        * `xml_bodleian` contains the metadata records from the Oxford Libraries
        * `xml_polonsky` contains the metadata records from the Polonsky project
    
    * `py` contains the python scripts 

        * `mss_quest_graphs.py` is mentioned in the paper as the *'first script'* (p.12). 
        * `mss_quest_graphs_wm.py` is mentioned in the paper as the *'second script'* (p.13).

    * `csv` contains the output CSV spreadsheets

        * `mss_quest_bodleian_graphs.csv` contains the data pulled out from the Oxford Libraries metadata records
        * `mss_quest_polonsky_graphs.csv` contains the data pulled out from the Polonsky project metadata records
        * `mss_quest_graphs_wm.csv` contains the data related to watermarks from the Polonsky project metadata record

* `mss_quest_maps` contains the scripts designed to output data suitable for creating geographic maps

    * `xml`contains the XML-TEI input metadata

        * `xml_bodleian` contains the metadata records from the Oxford Libraries
        * `xml_polonsky` contains the metadata records from the Polonsky project
        
    * `py` contains the python scripts 
        * `mss_quest_graphs.py` is mentioned in the paper as the *'third script'* (p.13).
        
    * `csv` contains the output CSV spreadsheets

        * `mss_quest_bodleian_maps.csv`contains the data pulled out from the Oxford Libraries metadata records
        * `mss_quest_polonsky_maps.csv`contains the data pulled out from the Polonsky project metadata records
        
        
### How the scripts work

The scripts are meant to be reusable, adaptable, and accessible. Even though they parse precise data structures and extract precise information from precise datasets, the scripts are delivered as code snippets that can easily be adapted to other datasets, mixed up to process multiple data at once, and implemented to obtain more complex outputs. Each code snippet comes with a consistent commentary apparatus, which explains in  less technical language what the code does line by line; this is meant for the code to be conceptually understood by those who are unfamiliar to Python or programming languages in general.

The scripts share some technical and methodological features. Firstly, they are all based on the BeautifulSoup library for Python. Secondly, no matter what specific information they are designed to pull out, all the scripts pull out the filename of each metadata record and the title of each described manuscript. Having the filenames in the outputs speeds up and lightens the workflow, because they allow checking of the output data (the extracted information is not always correct in respect to the metadata records), and also detection of the source of errors if the script returns some. The manuscript title can also be used as a reference to data throughout the workflow, but they are mostly useful as a ‘label’: most of the tools for digital outputs assign one to the visual representation of data (be they spots on a map, or events in a timeline, or points in a graph). Finally, all the output data sets consist in one or more CSV spreadsheets per script: CSV was chosen as the output format because it proves to be the most efficient format for data exchange across digital tools.  

Besides these common features, each script is designed to pull out specific information. The first script parses the Polonsky and the Oxford Libraries metadata at the same time and pulls out information related to manuscript contributors. It pulls out the names and roles of all persons that are mentioned in the records for having had a role in the history of the manuscripts (owners, donors, scribes...), as well as other information about the manuscripts, such as the form (codex, roll), material (paper, parchment, papyrus), place of origin and century of origin. Both Polonsky and Oxford Libraries metadata use Virtual International Authority File (VIAF) identifiers for names and short codes provided by the  Library Of Congress (LOC) for roles. Instead of providing the names and roles as they are expressed in the metadata records, the script outputs standard information provided by authority notices online: names come from the Resource Description Framework (RDF) records associated with VIAF identifiers, while roles come from the dedicated webpage of the LOC. The century of origin is meant to be approximate information: while the metadata records provide precise dates, the script converts them into the corresponding century. This is because the output data here is meant to be used for a general distanced reading of the corpus. The script outputs two distinct CSV spreadsheets, where each line corresponds to one contributor: the result is 94 contributors in Polonsky metadata and 12,121 in the whole set of Oxford Libraries metadata.  

The second script is designed to pull out information related to places. It also parses the Polonsky and Oxford Libraries metadata at the same time. It outputs the places of origin, the geographical coordinates, as well as the forms, materials and dates of origin; also, it looks for related resources online, such as TEI metadata records, IIIF records and Pinakes records, and returns the URIs of existing resources.  Both Polonsky and Oxford Libraries metadata use place identifiers from the TGN database. While the names of places come from the metadata records, the latitude and longitude are pulled out from the RDF record associated with the authority record in the Getty Thesaurus. This script also outputs two distinct CSV spreadsheets, where each line corresponds to one place of origin: the results is 10 names of places in Polonsky metadata and 6,368 in Oxford Libraries metadata.  

The last script only parses the Polonsky metadata, because the Oxford Libraries metadata set does not have a specific encoding model for watermarks. It pulls out the forms and materials, watermarks, places of origin and dates of origin of the manuscripts. The result is one CSV spreadsheet with 44 lines, which corresponds to the amount of watermarks encoded in the Polonsky metadata set.  

Why is it necessary to create separate scripts for each type of information? The approach adopted here is tool-oriented and its purpose is to make the processing task more agile and effective. The different outputs do not exclude each other: in fact, the CSV outputs for persons and watermarks also include the places of origin. However, while the scripts for persons and watermarks are meant to produce data for relational graphs, the output data of the scripts for places are meant to be fed into tools for geographical mapping. Geographical mapping requires specific information, such as latitude and longitude that is of little use in relational graphs. So, on the one hand the scripts designed for persons and watermarks pull out contents that can be represented in relational graphs; on the other hand, the script designed for places pulls out the information required for geographical mapping, which means not only the places of origin, but also the latitude and longitude, along with additional contents that can be displayed in pop-up bullets. Since the scripts parse data from consistent datasets (27 records from Polonsky and 10,250 from Oxford Libraries) and multiple online resources (Pinakes, VIAF, LOC, TGN), separating their functionalities in a tool-oriented approach makes the parsing quicker, the output spreadsheets lighter, and digital visualisations more readable.  

### Testing the scripts with the original metadata

1. Clone this repository on you local machine
    * With SSH: [git@gitlab.com:m.d.cristache/questioning-manuscript-metadata.git]() 
    * With HTTPS: [https://gitlab.com/m.d.cristache/questioning-manuscript-metadata.git]()

2. Launch the scripts

3. Be patient! The scripts parse a large quantity of input data and parse multiple online resources at the same time; once the scripts are done parsing, the CSV spreadsheets are complete.

4. Test the graphs with Palladio. Test the maps with Google Earth Pro.

### Testing the scripts with new metadata

1. Clone this repository on you local machine
    * With SSH: [git@gitlab.com:m.d.cristache/questioning-manuscript-metadata.git]() 
    * With HTTPS: [https://gitlab.com/m.d.cristache/questioning-manuscript-metadata.git]()  

2. In the folder `xml`: delete the contents of the folder and replace them with your own metadata set.

3. In the folder `csv`: delete all the contents of the folder.

3. In the folder `py`: update the scripts. 
    * update the relative path to input data (structure of the `xml` folder)  
    * update the relative path to output files (structure of the `csv` folder)  





