#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 18:09:39 2020

@author: kapu
"""

#-------------------------------------------------------------------------------------------

# The BeautifulSoup library parses the element trees of the XML input files
from bs4 import BeautifulSoup

# The re module provides regular expressions matching operations
import re

# The glob module generates lists of files matching given patterns (file paths)
import glob

import os

# The pathlib module offers a set of classes to handle filesystem paths
from pathlib import Path

# The rdflib library allows interactions with RDF
import rdflib

import urllib.request
from urllib.error import HTTPError

# the datetime module allows working with dates, times and time intervals
from datetime import datetime

import requests
#-------------------------------------------------------------------------------------------

__author__ = "Diandra Cristache"
__copyright__ = " "
__credits__ = ["Cambridge Digital Library"]
__license__ = "Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) "
__maintainer__ = "Diandra Cristache"
__email__ = "diandra.cristache@etu.univ-tours.fr"
__status__ = "Production"

#-------------------------------------------------------------------------------------------

delimiter = ","


# -------------------------------------------------------------------
# -------------------------------------------------------------------
# ---------------------- POLONSKY METADATA --------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
                   
print("FILENAME",delimiter,"TITLE",delimiter,"FORM",delimiter,"SUPPORT",delimiter,"PLACE",delimiter,"LAT",delimiter,"LON",delimiter,"DATE",delimiter,"MIN DATE",delimiter,"MAX DATE",delimiter,"TEI",delimiter,"IIIF",delimiter,"PINAKES",file=open("../csv/mss_quest_polonsky_maps.csv", "w"))

corpus = glob.glob('../xml/xml_polonsky/*/*.xml')

for file in corpus:

    # Open the original file 
    # and store the reference in the variable 'file_open'
    file_open = open(file)

    # Read the file content 
    # and store it in the variable 'file_content'
    file_content = file_open.read()

    # Give BeautifulSoup access to the file content 
    # and store the access in the variable 'soup'
    soup = BeautifulSoup(file_content, "xml")

    # Close the original file
    file_open.close()
    
    # Pull out the name of current file
    # from the local file path
    filename=os.path.splitext(os.path.basename(file))[0]
        
    # Parse each file of the corpus, while it is open in reading mode
    with open(file, 'r', encoding="utf-8") as content:
        
        # ----------------------------------------------------
        # ---------------- LINKS TO TEI METADATA -------------
        # ----------------------------------------------------
        
        try:
            uriTEI = "https://services.cudl.lib.cam.ac.uk/v1/metadata/tei/{}".format(filename)
            request = requests.get(uriTEI)
            if request.status_code == 200: 
                uriTEI = uriTEI
            else:
                uriTEI = None
        except FileNotFoundError:
            uriTEI= None
        
        # ----------------------------------------------------
        # ---------------- LINKS TO IIIF METADATA ------------
        # ----------------------------------------------------
            
        try:
            uriIIIF = "https://cudl.lib.cam.ac.uk/iiif/{}".format(filename)
            request = requests.get(uriIIIF)
            if request.status_code == 200:
                uriIIIF = uriIIIF
            else:
                uriIIIF = None
        except FileNotFoundError:
            uriIIIF= None
            
        # ----------------------------------------------------
        # ---------------- LINK TO PINAKES NOTICE -------------
        # ----------------------------------------------------
        
        pinakes = soup.find_all('idno',{'type':'diktyon'})
        
        for a in pinakes:
            try:
                pinakesID = ' '.join(a.get_text().split())
                uriPINAKES = "https://pinakes.irht.cnrs.fr/notices/oeuvre/{}/".format(pinakesID)
                request = requests.get(uriPINAKES)
                if request.status_code == 200:
                    uriPINAKES = uriPINAKES
                else:
                    uriPINAKES = None
            except FileNotFoundError:
                uriPINAKES= None
        
        # ----------------------------------------------------
        # ---------------- PULL OUT THE TITLE ----------------
        # ----------------------------------------------------
        
        mssTitle = soup.select('titleStmt title')
        for a in mssTitle:
            title = ' '.join(a.get_text().split())
        
        # ----------------------------------------------------
        # ---------------- PULL OUT THE FORM ----------------
        # ----------------------------------------------------

        mssForm = soup.select('msDesc physDesc objectDesc')
            
        if mssForm:
            for a in mssForm:
                try:
                    form = a['form']
                except KeyError:
                    form = None
        else:
            form = None
        # ----------------------------------------------------
        # ---------------- PULL OUT THE SUPPORT ----------------
        # ----------------------------------------------------
        
        mssSupport = soup.select('msDesc physDesc objectDesc supportDesc')
        
        if mssSupport:
            for a in mssSupport:
                try:
                    support = a['material']
                    if support == 'chart':
                        support = 'paper'
                    if support == 'perg':
                        support = 'parchment'
                except KeyError:
                    support = None
        else:
            support = None
                
        # ----------------------------------------------------
        # ---------------- PULL OUT THE DATE ----------------
        # ----------------------------------------------------
            
        mssOrigDate = soup.find_all('origDate')
        for a in mssOrigDate:
            
            if a :
            
                # ----------------MINIMUM DATE ----------------

                try:
                    minDate = a['notBefore']
                    minDate = re.sub('-.*$', '', minDate)    
                    #minDate = datetime.strptime(minDate, "%Y").date()    
                except KeyError:
                    minDate = None
                
                # ----------------MAXIMUM DATE ----------------
                
                try: 
                    maxDate = a['notAfter']
                    maxDate = re.sub('-.*$', '', maxDate)
                    #maxDate = datetime.strptime(maxDate, "%Y").date()
                except KeyError:
                    maxDate = None
                    
                # ----------------DATE ----------------
                
                try:    
                    when = a['when']
                    when = re.sub('-.*$', '', when)
                    #when = datetime.strptime(when, "%Y").date()
                except KeyError:
                    when = None
                    
            else:
                minDate = None
                maxDate = None
                when = None
                
        # ----------------------------------------------------
        # ---------------- PULL OUT THE PLACE ----------------
        # ----------------------------------------------------
        
        mssPlace = soup.find('origPlace')
            
        if mssPlace:
            
            for a in mssPlace:
                placeName = ' '.join(mssPlace.get_text().replace(',', ' -').split())
                placeKey = str(mssPlace['key']).replace('place_g','')
                
                try:
                    
                    uri = "http://vocab.getty.edu/page/tgn/{}.rdf".format(placeKey)
                    request = requests.get(uri)
                    
                    if request.status_code == 200:
                        g = rdflib.Graph()
                        parse = g.parse(uri)
        
                        # ---------------- PULL OUT THE COORDINATES ----------------
        
                        ask = parse.query(
                                """ASK {?x schema:latitude ?latitude .
                                        ?x schema:longitude ?longitude .
                                }""")
    
                        if ask:
                            
                            # ---------------- PULL OUT THE LATITUDE ----------------
                            
                            qres_lat = parse.query(
                                """SELECT ?latitude
                                   WHERE {
                                      ?x schema:latitude ?latitude .
                                   }""")
                            for row in qres_lat:
                                row_lat = "%s" %row
            
                            # ---------------- PULL OUT THE LONGITUDE ----------------
            
                            qres_lon = parse.query(
                                """SELECT ?longitude
                                   WHERE {
                                      ?x schema:longitude ?longitude .
                                   }""")
                            for row in qres_lon:
                                row_lon =  "%s" %row
                            
                        else:
                            row_lat = None
                            row_lon = None
                        
                    else:
                        row_lat = None
                        row_lon = None
                        
                except requests.ConnectionError as e:
                    print("Debug exception : ",e)
                    print("url: ",uri)
                    print("code: ",request.status_code)
                    row_lat = None
                    row_lon = None
                    
                # ----------------------------------------------------
                # ---------------- PRINT RESULTS ----------------
                # ----------------------------------------------------
                if row_lat and row_lon is not None:
                    print(filename,delimiter,title,delimiter,form,delimiter,support,delimiter,placeName,delimiter,row_lat,delimiter,row_lon,delimiter,when,delimiter,minDate,delimiter,maxDate,delimiter,uriTEI,delimiter,uriIIIF,delimiter,uriPINAKES,file=open("../csv/mss_quest_polonsky_maps.csv", "a"))
                
        else:
            placeName = None
            row_lat = None
            row_lon = None
            
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# ---------------------- BODLEIAN METADATA --------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
                    
print("FILENAME",delimiter,"TITLE",delimiter,"FORM",delimiter,"SUPPORT",delimiter,"PLACE",delimiter,"LAT",delimiter,"LON",delimiter,"DATE",delimiter,"MIN DATE",delimiter,"MAX DATE",delimiter,"TEI",delimiter,"IIIF",delimiter,"PINAKES",file=open("../csv/mss_quest_bodleian_maps.csv", "w"))

corpus = glob.glob('../xml/xml_bodleian/*/*.xml')

for file in corpus:

    # Open the original file 
    # and store the reference in the variable 'file_open'
    file_open = open(file)

    # Read the file content 
    # and store it in the variable 'file_content'
    file_content = file_open.read()

    # Give BeautifulSoup access to the file content 
    # and store the access in the variable 'soup'
    soup = BeautifulSoup(file_content, "xml")

    # Close the original file
    file_open.close()
    
    # Pull out the name of current file
    # from the local file path
    filename=os.path.splitext(os.path.basename(file))[0]
        
    # Parse each file of the corpus, while it is open in reading mode
    with open(file, 'r', encoding="utf-8") as content:
        
        # ----------------------------------------------------
        # ---------------- LINKS TO TEI METADATA -------------
        # ----------------------------------------------------
        
        uriTEI= None
            
        # ----------------------------------------------------
        # ---------------- LINKS TO IIIF METADATA ------------
        # ----------------------------------------------------
            
        uriIIIF= None
            
        # ----------------------------------------------------
        # ---------------- LINK TO PINAKES NOTICE -------------
        # ----------------------------------------------------
        
        pinakes = soup.find_all('ref',{'target':re.compile(r'http://pinakes.irht.cnrs.fr/notices/cote/.*')})
        
        for a in pinakes:
            try:
                uriPINAKES = a['target']
                request = requests.get(uriPINAKES)
                if request.status_code == 200:
                    uriPINAKES = uriPINAKES
                else:
                    uriPINAKES = None
            except ConnectionError:
                uriPINAKES= None
        
        # ----------------------------------------------------
        # ---------------- PULL OUT THE TITLE ----------------
        # ----------------------------------------------------
        
        mssTitle = soup.select('titleStmt title')
        for a in mssTitle:
            title = ' '.join(a.get_text().split())
        
        # ----------------------------------------------------
        # ---------------- PULL OUT THE FORM ----------------
        # ----------------------------------------------------

        mssForm = soup.select('msDesc physDesc objectDesc')
            
        if mssForm:
            for a in mssForm:
                try:
                    form = a['form']
                except KeyError:
                    form = None
        else:
            form = None
        # ----------------------------------------------------
        # ---------------- PULL OUT THE SUPPORT ----------------
        # ----------------------------------------------------
        
        mssSupport = soup.select('msDesc physDesc objectDesc supportDesc')
        
        if mssSupport:
            for a in mssSupport:
                try:
                    support = a['material']
                    if support == 'chart':
                        support = 'paper'
                    if support == 'perg':
                        support = 'parchment'
                except KeyError:
                    support = None
        else:
            support = None
                
        # ----------------------------------------------------
        # ---------------- PULL OUT THE DATE ----------------
        # ----------------------------------------------------
            
        mssOrigDate = soup.find_all('origDate')
        for a in mssOrigDate:
            
            if a :
            
                # ----------------MINIMUM DATE ----------------

                try:
                    minDate = a['notBefore']
                    minDate = re.sub('-.*$', '', minDate)
                except KeyError:
                    minDate = None
                
                # ----------------MAXIMUM DATE ----------------
                
                try: 
                    maxDate = a['notAfter']
                    maxDate = re.sub('-.*$', '', maxDate)
                except KeyError:
                    maxDate = None
                    
                # ----------------DATE ----------------
                
                try:    
                    when = a['when']
                    when = re.sub('-.*$', '', when)
                except KeyError:
                    when = None
                    
            else:
                minDate = None
                maxDate = None
                when = None
            
        # ----------------------------------------------------
        # ---------------- PULL OUT THE PLACE ----------------
        # ----------------------------------------------------
        
        mssPlace = soup.find('country',{'key':re.compile(r'.*')})
        
        if mssPlace:
            
            for a in mssPlace:
                placeName = ' '.join(mssPlace.get_text().replace(',', ' -').split())
                placeKey = str(mssPlace['key']).replace('place_','')
                    
                try:
                    uri = "http://vocab.getty.edu/page/tgn/{}.rdf".format(placeKey)
                    request = requests.get(uri)
                    if request.status_code == 200:
                        g = rdflib.Graph()
                        parse = g.parse(uri)
        
                        # ---------------- PULL OUT THE COORDINATES ----------------
                        
                        ask = parse.query(
                                """ASK {?x schema:latitude ?latitude .
                                        ?x schema:longitude ?longitude .
                                }""")
                        
                        if ask:
                        
                            # ---------------- PULL OUT THE LATITUDE ----------------
                            
                            qres_lat = parse.query(
                                """SELECT ?latitude
                                   WHERE {
                                      ?x schema:latitude ?latitude .
                                   }""")
                            for row in qres_lat:
                                row_lat = "%s" %row
            
                            # ---------------- PULL OUT THE LONGITUDE ----------------
            
                            qres_lon = parse.query(
                                """SELECT ?longitude
                                   WHERE {
                                      ?x schema:longitude ?longitude .
                                   }""")
                            for row in qres_lon: 
                                row_lon =  "%s" %row
                            
                        else:
                            row_lat = None
                            row_lon = None
                        
                    else:
                        row_lat = None
                        row_lon = None
                        
                except requests.ConnectionError as e:
                    print("Debug exception : ",e)
                    print("url: ",uri)
                    print("code: ",request.status_code)
                    row_lat = None
                    row_lon = None
                    
                # ----------------------------------------------------
                # ---------------- PRINT RESULTS ----------------
                # ----------------------------------------------------
                if row_lat and row_lon is not None:
                    print(filename,delimiter,title,delimiter,form,delimiter,support,delimiter,placeName,delimiter,row_lat,delimiter,row_lon,delimiter,when,delimiter,minDate,delimiter,maxDate,delimiter,uriTEI,delimiter,uriIIIF,delimiter,uriPINAKES,file=open("../csv/mss_quest_bodleian_maps.csv", "a"))
                
        else:
            placeName = None
            row_lat = None
            row_lon = None
    
